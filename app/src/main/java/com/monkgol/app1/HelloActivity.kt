package com.monkgol.app1

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.TextView


class HelloActivity : AppCompatActivity() {
    var showname: TextView? =null
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_hello)
        showname =findViewById<TextView>(R.id.Name1)
        var intent = intent
        showname!!.text = intent.getStringExtra("Name1")
        supportActionBar!!.title= "HELLO"
    }
}